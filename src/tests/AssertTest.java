import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.AssertionFailedError;

public class AssertTest {

	@Test(expected = AssertionFailedError.class)
	public void testFail_assertTrue() {
		junit.framework.Assert.assertTrue(1 > 2);
	}
	
	@Test(expected = AssertionFailedError.class)
	public void testFail_assertEquals() {
		String obj1="foo";
		String obj2="Foo";
		junit.framework.Assert.assertEquals(null,obj1, obj2);
	}

}
