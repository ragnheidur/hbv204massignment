# JUNIT 3.8.1

*  [![pipeline status](https://gitlab.com/ragnheidur/hbv204massignment/badges/master/pipeline.svg)](https://gitlab.com/ragnheidur/hbv204massignment/-/commits/master)
*  [![coverage report](https://gitlab.com/ragnheidur/hbv204massignment/badges/master/coverage.svg)](https://gitlab.com/ragnheidur/hbv204massignment/-/commits/master)
*  [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=ragnheidur_hbv204massignment&metric=alert_status)](https://sonarcloud.io/dashboard?id=ragnheidur_hbv204massignment)

*  This project uses Maven.
*  Have a look at the README.html file for more detailed version on JUNIT 3.8.1
*  To compile, run: mvn compile
*  To package, run: mvn package
*  To test, run: mvn test
